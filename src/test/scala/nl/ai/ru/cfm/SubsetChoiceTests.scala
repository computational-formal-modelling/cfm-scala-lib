package nl.ai.ru.cfm

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import nl.ru.ai.cfm.SubsetChoice._

@RunWith(classOf[JUnitRunner])
class SubsetChoiceTests extends FunSuite {
  test("subsetChoice: Should not add elements with value 0") {
    assert(subsetChoice(Set("a", "b", "c", "d"), Map("a" -> 1, "b" -> 2, "c" -> 0, "d" -> 3)) === Set("a", "b", "d"))
  }

  test("subsetChoice: Should not add elements with negative value") {
    assert(subsetChoice(Set("a", "b", "c", "d"), Map("a" -> 1, "b" -> 2, "c" -> -3, "d" -> 3)) === Set("a", "b", "d"))
  }

}
