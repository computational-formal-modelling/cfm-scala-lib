package nl.ru.ai.cfm

import nl.ru.ai.cfm.SetFun._

object SubsetChoice extends App {
  // We the type Choice to be equivalent to a String.
  type Choice = String

  /** The function subsetChoice computes a subset of the choices that maximizes the value of those choices.
    *
    * @param choices A set of choices.
    * @param valueFun A function that defines the value of each choice.
    * @return A maximum value subset.
    */
  def subsetChoice(choices: Set[Choice], valueFun: Choice => Int): Set[Choice] = {
    def valueFunBin(choice: Choice) = valueFun(choice)>0
    build(choices, valueFunBin)
  }

  // An example set of choices, e.g. pizza toppings. You are encouraged to try out your own.
  val toppings: Set[Choice] = Set("pineapple", "salami", "pepper", "olives")
  // An example value function
  val valueFun = Map(
    "pineapple" -> 1,
    "salami" -> -4,
    "pepper" -> 0,
    "olives" -> 2)

  println("Subset choice: "+subsetChoice(toppings, valueFun))

  /** The function binSubsetChoice computes a subset of the choices that maximizes the value of those choices,
    * taking into account interactions between pairs of choices.
    *
    * @param choices A set of choices.
    * @param valueFun1 A function that defines the value of each choice.
    * @param valueFun2 A function that defines the interaction value of a pair of choices.
    * @return A maximum value subset.
    */
  def binSubsetChoice(choices: Set[Choice],
                      valueFun1: Choice => Int,
                      valueFun2: ((Choice, Choice)) => Int): Option[Set[Choice]] = {
    def evalSet(choices: Set[Choice]): Int = {
      def excludeReflections(c1: Choice, c2: Choice): Boolean = c1 != c2
      sum(choices, valueFun1) + sum(cross(choices, choices, excludeReflections), valueFun2)
    }
    argmax(powerset(choices), evalSet)
  }

  // A smaller set of toppings.
  val toppings3: Set[Choice] = Set("pepperoni", "salami", "pineapple", "ham", "mushroom")
  // The value function of the choices in the smaller set.
  val valueFun3 = Map(
    "pepperoni" -> 2,
    "salami" -> 3,
    "pineapple" -> 2,
    "ham" -> 4,
    "mushroom" -> 1)
  // An example value function for pairs.
  val valueFun2 = Map(
    ("pepperoni","salami") -> -2,
    ("pineapple","salami") -> -7,
    ("pineapple","ham") -> 5,
    ("ham","salami") -> -3,
    ("pepperoni","ham") -> -4,
    ("pineapple","mushroom") -> -5
  ).withDefaultValue(0)

  val bestSubset = binSubsetChoice(toppings3, valueFun3, valueFun2)
  println("Binary subset choice: " + bestSubset)
  val orderedBestSubset = bestSubset.get.toList
  val unaryUtilities = orderedBestSubset.map(valueFun3)
  println("Has unary utility value: " + (orderedBestSubset zip unaryUtilities).mkString(" + ") + " = " + unaryUtilities.sum)

  val binaryOrderedSubsets = cross(bestSubset.get,bestSubset.get).toList
  val binaryUtilities = binaryOrderedSubsets.map(valueFun2)
  println("Has binary utility value: " + (binaryOrderedSubsets zip binaryUtilities).mkString(" + ") + " = " + binaryUtilities.sum)
}
