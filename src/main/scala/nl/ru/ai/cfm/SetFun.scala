package nl.ru.ai.cfm

import scala.util.Random

object SetFun {

  def build[U](set: Set[U], f: U => Boolean): Set[U] = {
    if(set.nonEmpty) {
      set.foldLeft(Set[U]()){(acc: Set[U], item: U) =>
        if(f(item)) acc + item
        else acc
      }
    } else Set.empty
  }

  def bigUnion[A](setOfSets: Set[Set[A]]): Set[A] = if(setOfSets.nonEmpty) setOfSets.reduce(_ union _) else Set.empty

  def bigIntersection[A](setOfSets: Set[Set[A]]): Set[A] = if(setOfSets.nonEmpty) setOfSets.reduce(_ intersect _) else Set.empty

  def cross[A](set1: Set[A], set2: Set[A]): Set[(A,A)] = for(x <- set1; y <- set2) yield (x,y)

  def cross[A](set1: Set[A], set2: Set[A], cond: (A, A) => Boolean): Set[(A,A)] = for(x <- set1; y <- set2 if cond(x,y)) yield (x,y)

  def forallUniquePairs[A](set: Set[A], cond: (A, A) => Boolean): Boolean = cross(set, set, (x: A, y: A)=>x!=y).forall((pair: (A,A)) => cond(pair._1, pair._2))

  def sum[A](set: Set[A], fun: A => Int): Int = (set map fun).sum

  def prod[A](set: Set[A], fun: A => Int): Int = (set map fun).product

  def fapply[A,B](set: Set[A], fun: A => B): Set[B] = set map fun

  def partitions[A](set: Set[A]): Set[Set[Set[A]]] = {
    def partitions(list: Seq[A]): Set[Set[Set[A]]] = {
      list match {
        case head::Nil => Set(Set(Set(head)))
        case head::tail =>
          val possiblePartitions = partitions(tail)
          val case1 = for(partition <- possiblePartitions) yield partition + Set(head)
          val case2 = for(partition <- possiblePartitions; part <- partition) yield (partition - part) + (part + head)
          case1 union case2
      }
    }
    partitions(set.toList)
  }

  def powerset[A](t: Set[A]): Set[Set[A]] = {
    @annotation.tailrec
    def pwr(t: Set[A], ps: Set[Set[A]]): Set[Set[A]] =
      if (t.isEmpty) ps
      else pwr(t.tail, ps ++ (ps map (_ + t.head)))
    pwr(t, Set(Set.empty[A]))
  }

  def argmax[A](set: Set[A], f: A => Int): Option[A] = {
    val seq = set.toSeq // convert to sequence to preserve ordering in zip function
    val valSeq = seq map f
    val maxValue = valSeq.max
    val maxValSet = seq zip valSeq filter (_._2 == maxValue)
    if(maxValSet.nonEmpty) Some(maxValSet(new Random().nextInt(maxValSet.length))._1) // if one or more maxima exist return random
    else None
  }


}
