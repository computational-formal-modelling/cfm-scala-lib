package nl.ru.ai.cfm

import nl.ru.ai.cfm.SetFun._

object Categorization extends App {
  // We define the type Thing to be equivalent to String.
  type Thing = String

  /** The function sim operates on a pair of things and calculates the similarity score between the two things
    * case on the hamming distance between the string representations.
    *
    * @param pair The pair of Things in a 2-tuple
    * @return A number representing the similarity between the two things.
    */
  def sim(pair: (Thing, Thing)): Int = (pair._1 zip pair._2).foldLeft(0)((acc, cp) => acc + (if(cp._1 == cp._2) 1 else 0))

  /**T he function disSim operates on a pair of things and calculates the dissimilarity score between the two things
    * case on the inverse hamming distance between the string representations.
    *
    * @param pair The pair of Things in a 2-tuple
    * @return A number representing the dissimilarity between the two things.
    */
  def dissim(pair: (Thing, Thing)): Int = (pair._1 zip pair._2).foldLeft(0)((acc, cp) => acc + (if(cp._1 != cp._2) 1 else 0))

  /** The categorization function returns (if it exists) a categorization of the things: Set[Thing] by maximizing
    * within category similarity and minimizing between category dissimilarity. You may want to define a helper
    * function that computes the categorizationScore given sim and dissim.
    *
    * @param things A set of things that needs to be categorized.
    * @param sim A function that defines the similarity between a pair of things.
    * @param dissim A function that defines the dissimilarity between a pair of things.
    * @return An optimal categorization.
    */
  def categorization(things: Set[Thing], sim: ((Thing, Thing)) => Int, dissim: ((Thing, Thing)) => Int): Option[Set[Set[Thing]]] = {
    def categorizationScore(categorization: Set[Set[Thing]]): Int = {
      def withinCategorySim(category: Set[Thing]): Int = sum(cross(category, category), sim)
      def betweenCategoryDisSim(categoryPair: (Set[Thing], Set[Thing])): Int = sum(cross(categoryPair._1, categoryPair._2), dissim)

      val sumDisSimilarityScore = sum(cross(categorization, categorization), betweenCategoryDisSim)
      val sumSimilarityScore = sum(categorization, withinCategorySim)

      sumSimilarityScore - sumDisSimilarityScore
    }

    argmax(partitions(things), categorizationScore)
  }

  // An example set of things, you are encouraged to try out your own. Try large sets at your own risk.
  val things = Set[Thing](
    "AAA", "AAB", "BBA", "CCB", "CBB")
  println(categorization(things, sim, dissim))

}
