package nl.ru.ai.cfm.graph {

  object Vertex {
    def apply[A](content: A): Vertex[A] = new Vertex[A](content)
  }

  class Vertex[A](val content: A) {
    def ~[B](that: Vertex[A], w: B = true): List[(A, A, B)] = List((this.content, that.content, w), (that.content, this.content, w))
    def d[B](that: Vertex[A], w: B = true): List[(A, A, B)] = List((this.content, that.content, w))
  }

}
