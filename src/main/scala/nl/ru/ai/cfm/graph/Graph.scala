package nl.ru.ai.cfm.graph {

object Graph {
  def apply[NodeType, EdgeType](graph: List[(NodeType, NodeType, EdgeType)])(implicit ordering: Ordering[NodeType]): DirectedGraph[NodeType, EdgeType] = {
    val mappings = graph
    val (from, to, link) = mappings.unzip3
    val vertices = (from.toSet union to.toSet).toVector.sorted
    def nodeToId(n: NodeType): Int = vertices.indexOf(n)

    var edges: Vector[Vector[EdgeType]] = Vector[Vector[EdgeType]]()
    // Initialize matrix
    for(i <- vertices.indices)
      for(j <- vertices.indices)
        edges = edges updated (i, edges(i) updated (j, new EdgeType))

    for(k <- from.indices)
      edges = edges updated (nodeToId(from(k)), edges(nodeToId(from(k))) updated (nodeToId(to(k)), link(k)))

    new DirectedGraph[NodeType, EdgeType](vertices, edges)
  }

    def apply[NodeType, EdgeType](graph: List[List[(NodeType, NodeType, EdgeType)]])(implicit ordering: Ordering[NodeType]): UndirectedGraph[NodeType, EdgeType] = {
      val mappings = graph.flatten
      val (from, to, link) = mappings.unzip3
      val vertices = (from.toSet union to.toSet).toVector.sorted
      def nodeToId(n: NodeType): Int = vertices.indexOf(n)

      var edges: Vector[Vector[EdgeType]] = Vector[Vector[EdgeType]]()
      // Initialize matrix
      for(i <- vertices.indices)
        for(j <- vertices.indices)
          edges = edges updated (i, edges(i) updated (j, new EdgeType))

      for(k <- from.indices)
          edges = edges updated (nodeToId(from(k)), edges(nodeToId(from(k))) updated (nodeToId(to(k)), link(k)))

      new UndirectedGraph[NodeType, EdgeType](vertices, edges)
    }
  }

  class Graph[NodeType, EdgeType](vertices: Vector[NodeType], edges: Vector[Vector[EdgeType]]) {
    override def toString: String = {
      vertices.mkString("\t") + "\n" + edges.mkString("\n")
    }
  }

  case class DirectedGraph[NodeType, EdgeType](vertices: Vector[NodeType], edges: Vector[Vector[EdgeType]])
    extends Graph(vertices: Vector[NodeType], edges: Vector[Vector[EdgeType]]) {
  }

  case class UndirectedGraph[NodeType, EdgeType](vertices: Vector[NodeType], edges: Vector[Vector[EdgeType]])
    extends Graph(vertices: Vector[NodeType], edges: Vector[Vector[EdgeType]]) {
  }

}