import nl.ru.ai.cfm.SetFun._

// We define a type Thing to be equivalent to String
type Thing = String

// A Set of Things {A, B, C} can be defined as follows
val s1 = Set[Thing]("A", "B", "C")

// Let's define a few more sets of Things
val s2 = Set[Thing]("B", "C", "D")
val s3 = Set[Thing]("X","Y","Z")

// We can use normal set operators like so
s1 union s2
s1 intersect s2
s1 intersect s3
s1 diff s3
s1 - "A"

// We can also use more powerful set operators
// Build is equivalent to set builders {x in S | condition(x)}
// This returns a set with all element in S for which some condition holds.
// The condition is passed along to set as a function.
def startsWithA(x: Thing): Boolean = x.startsWith("A")
build(s1, startsWithA)

// Big union returns the union of all sets in a set
bigUnion(Set(s1, s2, s3))

// Big intersection returns the intersection of all sets in a set
bigIntersection(Set(s1,s2))

// Cross returns the cardinal product of two sets
cross(s1, s3)
cross(s1, s2)
cross(s1, s1)


// Conditional cross returns the cardinal product of two sets
// where each pair (x \in s1, y \in s2) satisfies a condition.
// The condition is passed along to set as a function.
def notEqual(x: Thing, y: Thing): Boolean = x!=y
cross(s1, s3, notEqual)
cross(s1, s2, notEqual)

// Sum computes the sum of f(x) for all x in a set
def toNumber(x: Thing): Int = x match {
  case "A" => 1
  case "B" => 2
  case "C" => 3
  case "D" => 4
  case "X" => 24
  case "Y" => 25
  case "Z" => 26
}
sum(s1, toNumber)
sum(s2, toNumber)
sum(s3, toNumber)

// Product computes the product of f(x) for all x in a set
prod(s1, toNumber)
prod(s2, toNumber)
prod(s3, toNumber)

// You can combine these functions to construct complex set-theoretic expressions
sum(bigUnion(Set(s1, s2, s3)), toNumber)

// You can also map the items in a set to another domain
// We already have a function that maps Things to Int toLetterNumber
// The function fapply takes a set and a function and returns a set
// with the mapped elements.
fapply(s1, toNumber)
fapply(s2 union s3, toNumber)

// Suppose we want to find the Thing that has the highest number
// We can use the function argmax. Note in case of an empty set
// argmax has no valid answer, hence it will return None. If a valid
// answer exists is returns Some(answer) and if multiple maxima exist
// it returns one at random.
argmax(s2 union s3, toNumber)

// To construct the powerset of a set use
// Use this with care, the number of possible subsets is exponential.
powerset(s1)

// To construct all possible partitions of a set use
// Use this with care, the number of possible partitions is exponential.
partitions(s1)