{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Subset choice\n",
    "In this assignment you will develop your own computational-level model of subset choice, i.e., the task of choosing a subset of alternatives from a set of available alternatives. Some examples of subset choice are:\n",
    "\n",
    "- Selecting people to serve on a committee\n",
    "- A doctor prescribing medicine\n",
    "- Inviting people to a party\n",
    "- Doing groceries\n",
    "- Selecting toppings on a pizza\n",
    "\n",
    "A common assumption in decision-making is that people choose an option of maximum (or satisfactory) value.\n",
    "\n",
    "*Your task:* To propose a formal computational-level theory of subset choice that incorporates this assumption and explore its behaviour.\n",
    "\n",
    "We will work in three steps:\n",
    "1. Write an informal definition of the input-output mapping\n",
    "2. Write a formal definition of the input-output mapping\n",
    "3. Write a computer simulation of subset choice as defined in (2) and explore its behaviour\n",
    "\n",
    "## Example of subset choice: Pizza toppings\n",
    "Imagine you want to order a pizza. This particular restaurant offers customers the option to build their own pizza by selecting a combination of toppings based on their personal preferences. The joy of choice! Let's say there are five toppings on offer: *pepperoni*, *salami*, *pineapple*, *ham* and *mushroom*. You probably have a (dis)liking for each of these, in decision theory terms each toppings has a so-called utility $U$. For example:\n",
    "\n",
    "- $U(pepperoni)=2$\n",
    "- $U(salami)=3$\n",
    "- $U(pineapple)=2$\n",
    "- $U(ham)=4$\n",
    "- $U(mushroom)=1$\n",
    "\n",
    "### Practise 1\n",
    "What would be the best pizza you can get according to these utility values? Add the combination of toppings below:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Your answer (double click to edit)**\n",
    "\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Our answer (double click or select text to view)**\n",
    "\n",
    "<span style=\"color:white\">\n",
    "The best pizza we can get is $\\{pepperoni, salami, pineapple, ham, mushroom\\}$, because all toppings have positive utility we add them all.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it might be realistic to assume that certain combinations of toppings are better than others. Who likes pineapple and salami, right? So far, we have only considered unary utility values and our theory ignores possible *binary utility*. For example:\n",
    "\n",
    "- $U(pepperoni, salami)=-2$\n",
    "- $U(pineapple, salami)=-7$\n",
    "- $U(pineapple, ham)=5$\n",
    "- $U(ham, salami)=-3$\n",
    "- $U(pepperoni, ham)=-4$\n",
    "- $U(pineapple, mushroom)=-5$\n",
    "\n",
    "### Practise 2\n",
    "\n",
    "Let's assume that pairs of toppings that have no utility defined have utility 0. What would now be the best pizza you can get according to all unary and binary example values? Add the combination of toppings below:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Your answer (double click to edit)**\n",
    "\n",
    "..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Our answer (double click or select text to view)**\n",
    "\n",
    "<span style=\"color:white\">\n",
    "The best pizza we can get now is different from before: $\\{pineapple, ham\\}$. The total utility is $11$, where the unary utility is $2 + 4 = 6$ and the binary utility is $5$. However, we can only be sure this is the best pizza if we check that all other subset choices have lower utility than this one.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 1: Informal definition of subset choice\n",
    "By now you may have an intuition about the nature of subset choice. People's choices probably take into account at least unary and binary utility of possible alternatives. At this point, you can start working towards formalizing the computational-level problem that people solve when making choice decisions. Such a problem is characterized by the *input* that it receives and the *output* that is produced.\n",
    "\n",
    "### Practise 3\n",
    "In your own words, informally describe what information is required in the input of subset choice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Your answer (double click to edit)**\n",
    "\n",
    "*Input:* ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, also in your own words, informally describe what the output of subset choice is, in terms of the input you have just specified. Note: While describing the output, you may realize that you need to add something to the input of your computational-level theory. In that case, simply go back and update your answer above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Your answer (double click to edit)**\n",
    "\n",
    "*Output:* ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Our answer (double click or select text to view)**\n",
    "\n",
    "<span style=\"color:white\">\n",
    "There are many different ways in which one can formalize subset choice. Here, we provide one possible formalization. It is by no means 'more correct' than another formalization, but it may have different assumptions.\n",
    "\n",
    "**Subset choice (informal)**\n",
    "\n",
    "*Input:* A set of choice options, each choice option has a unary utility and each pair of choice options has a binary utility. We assume that the binary utility of an option and itself is $0$.\n",
    "\n",
    "*Output:* A subset of choice options from the total set, such that the sum of the unary utility of each choice plus the sum of the binary utility of each pair is maximal, or nothing if the set of choice options is empty.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Go ahead and compare your answer to ours. What are the differences? What do the differences mean? Do they matter?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 2: Formal definition of subset choice\n",
    "\n",
    "Based on your informal definition, write a formal definition of subset choice. Again, you will need to specify the *input* and *output* of subset choice, but this case you should use math to make the definition as unambiguous as possible. It should be well specified enough such that two different persons reading it cannot interpret it differently.\n",
    "\n",
    "For this exercise we recommend you use set theory, but feel free to use different math if you can make it work. A small recap of a subset of set-theoretic notation (edit this cell to view latex code):\n",
    "\n",
    "- **Set notation:** $S$ consists of elements $\\{s_1,\\dots,s_n\\}$\n",
    "- **Set union:** $\\{a,b,c\\} \\cup \\{c,d,e\\} = \\{a,b,c,d,e\\}$\n",
    "- **Set intersection:** $\\{w,x,y\\} \\cap \\{x,y,z\\} = \\{x,y\\}$\n",
    "- **Set difference:** $\\{w,x,y\\} \\setminus \\{x,y,z\\} = \\{w\\}$ and $\\{x,y,z\\} \\setminus \\{w,x,y\\} = \\{z\\}$\n",
    "- **Symmetric difference:** $\\{w,x,y\\} \\triangle \\{x,y,z\\} = \\{w,z\\}$\n",
    "- **Cartesian product:** All ordered pairs between two subsets, e.g., $\\{a,b,c\\} \\times \\{y,z\\} = \\{(a,y),(a,z),(b,y),(b,z),(c,y),(c,z)\\}$\n",
    "- **Power set:** A set whose elements are all possible subsets, e.g., $\\mathcal{P}(\\{a,b,c\\})=\\{\\{a\\},\\{b\\},\\{c\\},\\{a,b\\},\\{a,c\\},\\{b,c\\},\\{a,c\\},\\{a,b,c\\}\\}$\n",
    "- **Conditional set constructor:** A subset whose elements satisfy a conditional function, e.g., $\\forall_{i\\in A}\\left[i~\\text{if}~f(i)>0\\right]$\n",
    "\n",
    "### Practise 4\n",
    "\n",
    "Write a formal computational-level definition of the informal definition you provided in Practise 3. If you were not able to complete Practise 3, we do not recommend to proceed until you have done so."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Your answer (double click to edit)**\n",
    "\n",
    "*Input:* ...\n",
    "\n",
    "*Output:* ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Our answer (double click or select text to view)**\n",
    "\n",
    "<span style=\"color:white\">\n",
    "\n",
    "**Subset choice (informal)**\n",
    "\n",
    "*Input:* A set of choice options $C$, a unary utility function $u: C \\rightarrow \\mathbb{Z}$ a binary utility function $u: C \\times C \\rightarrow \\mathbb{Z}$, where $u(a,a)=0$.\n",
    "\n",
    "*Output:* A subset $C'\\subseteq C$ s.t. $\\sum_{a\\in C'}u(a) + \\sum_{a,b \\in C'} u(a,b)$ is maximal.\n",
    "</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, go ahead and compare your answer to ours. What are the differences? What do the differences mean? Do they matter?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 3: Computer simulation\n",
    "\n",
    "In this part you are going to write a small computer simulation program that simulates your formal computational-level definition of subset choice. You will use this to explore behaviour of your model. Simulations can help you identify behaviours of your model that are as intended, but also behaviours that are not. This is as much an exercise in coming up with test examples and desired properties as it is in understanding *exactly* what your formalization means.\n",
    "\n",
    "We provide a functionality related to set-theoretic constructs and operations. Run the code (select the cell and press Ctrl-Enter) in the cell below to load the library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scala.util.Random\n",
    "\n",
    "def build[U](set: Set[U], f: U => Boolean): Set[U] = {\n",
    "if(set.nonEmpty) {\n",
    "  set.foldLeft(Set[U]()){(acc: Set[U], item: U) =>\n",
    "    if(f(item)) acc + item\n",
    "    else acc\n",
    "  }\n",
    "} else Set.empty\n",
    "}\n",
    "\n",
    "def sdiff[A](s1: Set[A], s2: Set[A]): Set[A] = (s1 diff s2) union (s2 diff s1)\n",
    "\n",
    "def bigUnion[A](setOfSets: Set[Set[A]]): Set[A] = if(setOfSets.nonEmpty) setOfSets.reduce(_ union _) else Set.empty\n",
    "\n",
    "def bigIntersection[A](setOfSets: Set[Set[A]]): Set[A] = if(setOfSets.nonEmpty) setOfSets.reduce(_ intersect _) else Set.empty\n",
    "\n",
    "def cross[A](set1: Set[A], set2: Set[A]): Set[(A,A)] = for(x <- set1; y <- set2) yield (x,y)\n",
    "\n",
    "def cross[A](set1: Set[A], set2: Set[A], cond: (A, A) => Boolean): Set[(A,A)] = for(x <- set1; y <- set2 if cond(x,y)) yield (x,y)\n",
    "\n",
    "def forallUniquePairs[A](set: Set[A], cond: (A, A) => Boolean): Boolean = cross(set, set, (x: A, y: A)=>x!=y).forall((pair: (A,A)) => cond(pair._1, pair._2))\n",
    "\n",
    "def sum[A](set: Set[A], fun: A => Int): Int = (set map fun).sum\n",
    "\n",
    "def prod[A](set: Set[A], fun: A => Int): Int = (set map fun).product\n",
    "\n",
    "def fapply[A,B](set: Set[A], fun: A => B): Set[B] = set map fun\n",
    "\n",
    "def partitions[A](set: Set[A]): Set[Set[Set[A]]] = {\n",
    "def partitions(list: Seq[A]): Set[Set[Set[A]]] = {\n",
    "  list match {\n",
    "    case head::Nil => Set(Set(Set(head)))\n",
    "    case head::tail =>\n",
    "      val possiblePartitions = partitions(tail)\n",
    "      val case1 = for(partition <- possiblePartitions) yield partition + Set(head)\n",
    "      val case2 = for(partition <- possiblePartitions; part <- partition) yield (partition - part) + (part + head)\n",
    "      case1 union case2\n",
    "  }\n",
    "}\n",
    "partitions(set.toList)\n",
    "}\n",
    "\n",
    "def powerset[A](t: Set[A]): Set[Set[A]] = {\n",
    "@annotation.tailrec\n",
    "def pwr(t: Set[A], ps: Set[Set[A]]): Set[Set[A]] =\n",
    "  if (t.isEmpty) ps\n",
    "  else pwr(t.tail, ps ++ (ps map (_ + t.head)))\n",
    "pwr(t, Set(Set.empty[A]))\n",
    "}\n",
    "\n",
    "def argmax[A](set: Set[A], f: A => Int): Option[A] = {\n",
    "val seq = set.toSeq // convert to sequence to preserve ordering in zip function\n",
    "val valSeq = seq map f\n",
    "val maxValue = valSeq.max\n",
    "val maxValSet = seq zip valSeq filter (_._2 == maxValue)\n",
    "if(maxValSet.nonEmpty) Some(maxValSet(new Random().nextInt(maxValSet.length))._1) // if one or more maxima exist return random\n",
    "else None\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set-theory examples\n",
    "\n",
    "You can create a sets as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val s1 = Set('a', 'b', 'c')\n",
    "val s2 = Set('c', 'd')\n",
    "val s3 = Set('w', 'x', 'y')\n",
    "val s4 = Set('x', 'y', 'z')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set operators can be used as such:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val union = s1 union s2\n",
    "val intersection = s3 intersect s4\n",
    "val diff34 = s3 diff s4\n",
    "val diff43 = s4 diff s3\n",
    "val symmetricDiff = sdiff(s3, s4)\n",
    "val cartesian = cross(s1, s2)\n",
    "val powersetOfS1 = powerset(s1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use the set builder to build sets from other sets, it requires a function that evaluates to `True` or `False`. Here, we use a made up function that evaluates whether a letter is 'even' or not, based on its position in the alphabet. Obviously, this function assumes that sets consist of characters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def isEvenLetter(x: Char): Boolean = {\n",
    "    val alphabet = 'a' to 'z'\n",
    "    alphabet.indexOf(x.toLower) % 2 == 0\n",
    "}\n",
    "build(s1, isEvenLetter) union build(s3, isEvenLetter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Practise 5\n",
    "\n",
    "Use the functions above to implement your formal computational-level theory. In Scala, a function consists of *arguments*, a *type* and a *body*.\n",
    "\n",
    "```\n",
    "def f(..arguments..): ..type.. = {\n",
    "    ..body..\n",
    "}\n",
    "```\n",
    "\n",
    "The arguments specify the nature of the input of the function, e.g., this function takes as input an integer `i` and a string `j`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(i: Int, j: String) = ???"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The type of the function specifies the nature of its output, e.g., this function's output takes the form of an integer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def g(): Int = ???"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The body of the function specifies how the transformation from input to output is computed. You can think of the arguments as specifying the computational-level input, the type as specifying the nature of the computational-level output and the body as (one possible) means of computing the output as specified by the computational-level theory.\n",
    "\n",
    "Specify the simulation function of your computational-level theory below.\n",
    "\n",
    "**Your answer:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mySubsetChoice(..input..): ..output.. = {\n",
    "    ..transform input to output..\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Our answer:**\n",
    "In our answer we had three components in the input:\n",
    "\n",
    "1. A set of choice options: $C$\n",
    "2. A unary utility function: $u(a)$\n",
    "3. A binary utility function: $u(a,b)$\n",
    "\n",
    "For our simulation, we need to defined the types of these components:\n",
    "\n",
    "1. For simplicity, we assume $C$ consists of strings, its type is `Set[String]`\n",
    "2. The unary utility function takes a string and returns its utility value. Its type is: `String => Int`\n",
    "3. The binary utility function takes two strings and returns their utility value. Its type is: `(String, String) => Int`\n",
    "\n",
    "An example set of choices can be defined as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "val toppings = Set(\"pepperoni\", \"salami\", \"pineapple\", \"ham\", \"mushroom\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can code some example utility functions as follows. Since we are not aiming to explain where utility comes from, these can be implemented with a simple list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def u1(a: String): Int = {\n",
    "    a match {\n",
    "      case \"pepperoni\" => 2\n",
    "      case \"salami\" => 3\n",
    "      case \"pineapple\" => 2\n",
    "      case \"ham\" => 4\n",
    "      case \"mushroom\" => 1\n",
    "      case _ => 0\n",
    "    }\n",
    "}\n",
    "\n",
    "def u2(pair: (String, String)): Int = {\n",
    "    pair match {\n",
    "      case (\"pepperoni\", \"salami\") => -2\n",
    "      case (\"pineapple\", \"salami\") => -7\n",
    "      case (\"pineapple\", \"ham\") => 5\n",
    "      case (\"ham\", \"salami\") => -3\n",
    "      case (\"pepperoni\", \"ham\") => -4\n",
    "      case (\"pineapple\", \"mushroom\") => -5\n",
    "      case _ => 0\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def ourSubsetChoice(choices: Set[String], u1: String => Int, u2: ((String, String)) => Int): Set[String] = {\n",
    "    // We use a helper function that evaluates the utility score of any subset.\n",
    "    def evalSet(choices: Set[String]): Int = {\n",
    "        // Since we assumed we do not count any binary utility between an item and itself,\n",
    "        // we exclude reflections from the cardinal set.\n",
    "        def excludeReflections(c1: String, c2: String): Boolean = c1 != c2\n",
    "        // The sum of unary utilities\n",
    "        val u1Sum = sum(choices, u1)\n",
    "        // The sum of binary utilities\n",
    "        val u2Sum = sum(cross(choices, choices, excludeReflections), u2)\n",
    "        \n",
    "        u1Sum + u2Sum\n",
    "    }\n",
    "    \n",
    "    // Compute all possible subsets of choices\n",
    "    val allSubsets = powerset(choices)\n",
    "    \n",
    "    // Select the subset with the highest utility\n",
    "    argmax(allSubsets, evalSet).get\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ourSubsetChoice(toppings, u1, u2)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Scala 2.12",
   "language": "scala",
   "name": "scala212"
  },
  "language_info": {
   "codemirror_mode": "text/x-scala",
   "file_extension": ".scala",
   "mimetype": "text/x-scala",
   "name": "scala",
   "nbconvert_exporter": "script",
   "version": "2.12.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
